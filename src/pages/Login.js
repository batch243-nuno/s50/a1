import { Form, Button, Container, Row, Col } from 'react-bootstrap';

import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

const sampleUser = {
  email: 'sample@email.com',
  password: 'sample',
};

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disable, setDisable] = useState(true);

  // const [user, setUser] = useState(null);

  const { setUser } = useContext(UserContext);

  useEffect(() => {
    if (email && password) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [email, password]);

  const login = (event) => {
    event.preventDefault();
    if (email === sampleUser.email) {
      if (password === sampleUser.password) {
        setEmail('');
        setPassword('');
        alert(`${email} is Logged In!`);

        // set authentication in local storage
        localStorage.setItem('email', 'sample@email.com');
        setUser(localStorage.getItem('email'));
      } else {
        alert(`Wrong Password!`);
      }
    } else {
      alert(`Wrong Email!`);
    }
  };

  return (
    <Container fluid>
      <Row className="">
        <Col className="d-flex justify-content-center">
          <Form onSubmit={login} className="col-12 col-md-4">
            <Form.Group className="" controlId="formEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="" controlId="formPassword">
              <Form.Label>Enter Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="" controlId="formCheckbox"></Form.Group>
            <Button variant="primary" type="submit" disabled={disable}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
