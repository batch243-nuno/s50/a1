import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

function Register() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [disable, setDisable] = useState(true);

  useEffect(() => {
    if (email && password && rePassword && password === rePassword) {
      setDisable(false);

      // alert('Successfully Registered');

      // setDisable(true);
      // alert('Password Not Match!');
    } else {
      setDisable(true);
    }
  }, [email, password, rePassword]);

  const { setUser } = useContext(UserContext);

  const registerHandler = (event) => {
    event.preventDefault();
    setEmail('');
    setPassword('');
    setRePassword('');
    alert(`${email} is Successfully Registered`);

    localStorage.setItem('email', email);
    setUser(localStorage.getItem('email'));
  };

  return (
    <Container fluid>
      <Row className="">
        <Col className="d-flex justify-content-center">
          <Form onSubmit={registerHandler} className="col-12 col-md-4">
            <Form.Group className="mb-3" controlId="formEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formPassword">
              <Form.Label>Enter Your Desired Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formRePassword">
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Re-Enter Password"
                value={rePassword}
                onChange={(event) => setRePassword(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formCheckbox"></Form.Group>
            <Button variant="primary" type="submit" disabled={disable}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
