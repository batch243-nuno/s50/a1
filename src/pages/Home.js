import Banner from '../components/Banner';
import Highlight from '../components/Highlight';
import { Container } from 'react-bootstrap';

function Home() {
  return (
    <Container>
      <Banner />
      <Highlight />
    </Container>
  );
}

export default Home;
