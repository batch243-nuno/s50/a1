import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

function Logout() {
  const { setUser, unSetUser } = useContext(UserContext);
  useEffect(() => {
    setUser(unSetUser);
  }, []);

  return <Navigate to="/login" />;
}

export default Logout;
