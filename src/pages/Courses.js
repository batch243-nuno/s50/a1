import React from 'react';
import { Container, Row } from 'react-bootstrap';

import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses() {
  //   console.log();

  // localstorage.getItem
  // const local = localStorage.getItem('email');
  // console.log(local);

  return (
    <Container>
      <Row>
        {coursesData.map((course) => (
          <CourseCard key={course.id} coursesData={course} />
        ))}
      </Row>
    </Container>
  );
}
