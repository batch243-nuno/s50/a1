import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

import { useState } from 'react';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';

import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState(localStorage.getItem('email'));

  const unSetUser = () => {
    localStorage.removeItem('email');
  };
  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route
            path="/login"
            element={!user ? <Login /> : <Navigate to="/" />}
          />
          <Route
            path="/register"
            element={!user ? <Register /> : <Navigate to="/" />}
          />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<PageNotFound />} />
          {/* <Navigate from="*" to="/404" /> */}
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
