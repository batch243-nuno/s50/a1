import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Banner() {
  return (
    <Row className="">
      <Col className="">
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Oppurtunities for everyone, everywhere.</p>
        <Button as={Link} to="/courses">
          Enroll Now!
        </Button>
      </Col>
    </Row>
  );
}

export default Banner;
