import { Row, Col } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';

function Highlight() {
  return (
    <Row className="my-3">
      <Col xs={12} md={4}>
        <Card className="text-center p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>Learn From Home</h2>
            </Card.Title>
            <Card.Text>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Repellendus a voluptatibus totam repellat asperiores blanditiis
              autem quibusdam, esse, hic ad aspernatur commodi cumque numquam
              est odit consequuntur rem id iste.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="text-center p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>Study Now Pay Later</h2>
            </Card.Title>
            <Card.Text>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Repellendus a voluptatibus totam repellat asperiores blanditiis
              autem quibusdam, esse, hic ad aspernatur commodi cumque numquam
              est odit consequuntur rem id iste.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="text-center p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>Be Part Of Our Community</h2>
            </Card.Title>
            <Card.Text>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Repellendus a voluptatibus totam repellat asperiores blanditiis
              autem quibusdam, esse, hic ad aspernatur commodi cumque numquam
              est odit consequuntur rem id
              iste.sjdhgfjhsdgfhjsdgfsjhdfgsjhdfgsjdhfgsdjhfgsdhfgsdjhfgsjdhfgshdgfhsdg
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default Highlight;
